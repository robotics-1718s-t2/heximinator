import time, serial

jalad = [[20, 21, 22], [16, 17, 18], [0, 1, 2], [4,5,13], [7, 9, 10], [24, 25, 31]]

def stand(ser):
    ser.write()

def servoDetect(ser):
    for i in range(33):
        while True:
            inp = raw_input("::")
            if inp=="a":
                print "Break"
                break
            else:
                print i
                ser.write('#' + str(i) + " P1600 T1000 "+chr(13))
                time.sleep(2)
                ser.write('#' + str(i) + " P800 T1000 "+chr(13))
                time.sleep(3)
                ser.write(chr(27))

def flag(ser):
    #28
    for i in range(0,6):
        ser.write("#28 P1567 "+chr(13))
        time.sleep(0.4)
        ser.write("#28 P1450 "+chr(13))
        time.sleep(0.4)
    ser.write("#28 P1510 "+chr(13))
    time.sleep(0.1)
    ser.write("#28 P0 "+chr(13))

def moveFlagRight(ser):
    ser.write("#28 P1570 "+chr(13))

def moveFlagLeft(ser):
    ser.write("#28 P1450 "+chr(13))

def stopFlag(ser):
    ser.write("#28 P1510 "+chr(13))
    time.sleep(0.1)
    ser.write("#28 P0 "+chr(13))

def zero(ser, nr): #null on 1507 - 1510
    a = 1500
    servo = nr
    while True:
        inp = raw_input("::")
        if inp=="+":
            a += 10
        elif inp=="++":
            a += 100
        elif inp=="-":
            a -=10
        elif inp=="--":
            a -=100
        elif inp.isdigit():
            servo = inp
        print(a)
        ser.write("#" + str(servo) +" P" + str(a) +chr(13))

def riseLeg(ser): #jalg2
    ser.write("#17 P600 #18 P2200 T2000 "+chr(13))
    time.sleep(3)
    
def rise2Legs(ser): #jalg2 ja 5
    ser.write("#9 P600 #10 P2200 T2000 "+chr(13))
    ser.write("#17 P600 #18 P2200 T2000 "+chr(13))
    time.sleep(3)

def idleStanding(ser):
    for jalg in jalad:
        ser.write("#" + str(jalg[2]) + "P500"+chr(13))
        #930
        ser.write("#" + str(jalg[1]) + "P930"+chr(13))
        #1270
        ser.write("#" + str(jalg[0]) + "P1270"+chr(13))

def legStep(ser, nr):
    ser.write("#" + str(jalad[nr-1][1]) + "P500" + "#" + str(jalad[nr-1][0]) + "P1600 T100" + chr(13))
    time.sleep(0.2)
    ser.write("#" + str(jalad[nr-1][1]) + "P930 T50" + chr(13))
    time.sleep(0.5)
    #ser.write("#" + str(jalad[nr-1][0]) + "P1270 T50" +chr(13))
    #time.sleep(0.5)
def rotateBody(ser):
    ser.write("#" + str(jalad[0][0]) + "P1270 #" + str(jalad[2][0]) + "P1270 #" + str(jalad[3][0]) + "P1270 #" + str(jalad[5][0]) + "P1270 T100" +chr(13))
    time.sleep(0.5)
def tiltSide(ser, leg1, leg2, leg3):
    ser.write("#" + str(jalad[leg1-1][1]) + "P850" + "#" + str(jalad[leg2-1][1]) + "P850" + str(jalad[leg2-1][1]) + "P1000 T100" + chr(13))
    time.sleep(0.2)

def deTiltSide(ser, leg1, leg2, leg3):
    ser.write("#" + str(jalad[leg1-1][1]) + "P930" + "#" + str(jalad[leg2-1][1]) + "P930" + "#" + str(jalad[leg3-1][1]) + "P930 T100" + chr(13))
    time.sleep(0.2)
    
def walk(ser):
    #jalad vasakul 3,4 jalad paremal 1, 6
    #   1 I 3
    #   6   4
    #idleStanding(ser)
    #rise2Legs(ser)
    time.sleep(1)
    
    tiltSide(ser, 6, 4, 3)
    legStep(ser, 1)
    deTiltSide(ser, 6, 4, 3)
    tiltSide(ser, 1, 6, 4)
    legStep(ser, 3)
    deTiltSide(ser, 1, 6, 4)
    tiltSide(ser, 1, 3, 6)
    legStep(ser, 4)
    deTiltSide(ser, 1, 3, 6)
    tiltSide(ser, 4, 3, 1)
    legStep(ser, 6)
    deTiltSide(ser, 4, 3, 1)

    rotateBody(ser)

def tiltTest(ser):
    #idleStanding(ser)
    time.sleep(0.5)
    tiltSide(ser, 1, 3)
    time.sleep(2)
    deTiltSide(ser, 1, 3)

    tiltSide(ser, 3, 4)
    time.sleep(2)
    deTiltSide(ser, 3, 4)

    tiltSide(ser, 4, 6)
    time.sleep(2)
    deTiltSide(ser, 4, 6)

    tiltSide(ser, 6, 1)
    time.sleep(2)
    deTiltSide(ser, 6, 1)

def letLoose(ser):
    for i in range(32):
        ser.write("#" + str(i) + " P0" + chr(13))
