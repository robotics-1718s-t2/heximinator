import display, songAndFlag, serial, servoMovement, time

def fancyWalking(ser):
    servoMovement.walk(ser)

def waveFlag(ser):
    servoMovement.flag(ser)

def songLyrics():
    display.displaySong()

def disco():
    display.doDisco()

def riseOneLeg(ser):
    servoMovement.riseLeg(ser)

def riseTwoLegs(ser):
    servoMovement.rise2Legs(ser)

def flagAndSong(ser):
    songAndFlag.moveFlagAndDisplaySong(ser)



def chooseTask(ser, shape):
    if shape == 'circle':
        riseTwoLegs(ser)
        for i in range(4):
            fancyWalking(ser)
    elif shape == 'letterE':
        waveFlag(ser)
    elif shape == 'letterT':
        songLyrics()
    elif shape == 'numberONE':
        disco()
    elif shape == 'square':
        riseOneLeg(ser)
    elif shape == 'star':
        riseTwoLegs(ser)
    elif shape == 'triangle':
        flagAndSong(ser)
    servoMovement.idleStanding(ser)
'''    
try:
    ser = serial.Serial(
        port='/dev/ttyAMA0',
        baudrate=115200,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout=1)
except:
    serial.Serial('/dev/ttyAMA0', 115200).close()
    ser = serial.Serial(
        port='/dev/ttyAMA0',
        baudrate=115200,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout=1)
try:
    #flagAndSong(ser)
    
    #servoMovement.zero(ser, 25)
    servoMovement.idleStanding(ser)
    riseTwoLegs(ser)
    #time.sleep(2)
    while 1:
        servoMovement.walk(ser)
        #servoMovement.tiltTest(ser)
    #time.sleep(1)
    inp = raw_input("::")
    for i in range(0, 32):
        ser.write("#" + str(i) + " P0 "+chr(13))
except KeyboardInterrupt:
    #ser.write("#22 P1510 "+chr(13))
    time.sleep(0.1)
    for i in range(0, 32):
        ser.write("#" + str(i) + " P0 "+chr(13))
    ser.write(chr(27))
    ser.close()'''
    
