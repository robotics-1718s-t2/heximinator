import cv2

image = cv2.imread("beeSM.png")
bCanny = cv2.Canny(cv2.cvtColor(image.copy(), cv2.COLOR_BGR2GRAY), 50, 200)
cnts = cv2.findContours(bCanny.copy(), cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
c = max(cnts[0], key=cv2.contourArea)
cv2.drawContours(image, [c], -1, (0, 255, 255), 2)
cv2.imshow("bCanny", image)
while True:
    if (cv2.waitKey(10) & 0xFF == ord('q')):
            break

#video_device.release()
cv2.destroyAllWindows()
"""

image = cv2.imread("beeSM.png")
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

cnts = cv2.findContours(cv2.Canny(gray.copy(), 50, 200), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
cnts = cnts[0]
c = max(cnts, key=cv2.contourArea)
cv2.drawContours(image, [c], -1, (0, 255, 255), 2)
cv2.imshow("image", image)
cv2.waitKey(0)
"""
