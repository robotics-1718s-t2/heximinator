import os
import datetime
import Adafruit_CharLCD as LCD
import time
import random

def displaySong():
    lcd = LCD.Adafruit_CharLCDPlate()
    lcd.set_color(0,0,255)

    message = "     What is love? Baby don't hurt me, don't hurt me. No more. Baby don't hurt me, don't hurt me. No more. What is love? Yeah"
    start = -1
    stop = 15
    start2 = 0
    stop2 = 0
    lcd.message(message)
    while True:
        if lcd.is_pressed(LCD.SELECT):
            lcd.clear()
            break

        if start == len(message):
            start = 0
            stop = 16
            start2 = 0
            stop2 = 0        
            
        elif stop == len(message):
            if start - stop == 15:
                start2 = 0
                stop2 = 1
            else:
                start += 1

                stop2 += 1
        else:
            start += 1
            stop += 1
        print start, stop, start2, stop2
        dispMessage = message[start:stop] + message[start2:stop2]
        lcd.message(dispMessage)
        print dispMessage
        time.sleep(0.3)
        if lcd.is_pressed(LCD.SELECT):
            lcd.clear()
            break
        lcd.clear()
        time.sleep(0.1)
        
def doDisco():
    colors = [(0,0,255),(255,0,0),(0,255,0),(255,0,255),(0,255,255),(255,255,0),(255,255,255)]
    i = 0
    lcd = LCD.Adafruit_CharLCDPlate()
    lcd.set_color(0,0,255)
    while True:
        if i==len(colors):
            i=0
        lcd.set_color(colors[i][0],colors[i][1],colors[i][2])
        time.sleep(0.5)
        i += 1
        if lcd.is_pressed(LCD.SELECT):
            lcd.clear()
            break
    

