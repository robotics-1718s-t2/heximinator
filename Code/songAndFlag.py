import servoMovement, time
import Adafruit_CharLCD as LCD

def moveFlagAndDisplaySong(ser):
    lcd = LCD.Adafruit_CharLCDPlate()
    lcd.set_color(255,255,255)
    message = "     What is love? Baby don't hurt me, don't hurt me. No more. Baby don't hurt me, don't hurt me. No more. What is love? Yeah"
    start = -1
    stop = 15
    start2 = 0
    stop2 = 0
    #message = "SCROLL"
    lcd.message(message)
    delayMS = time.time()*1000 + 400
    suund = 0
    while True:
        if time.time()*1000 > delayMS:
            if suund == 0:
                suund = 1
                servoMovement.moveFlagLeft(ser)
            elif suund == 1:
                suund = 0
                servoMovement.moveFlagRight(ser)
            delayMS = time.time()*1000 + 400
        if lcd.is_pressed(LCD.SELECT):
            lcd.clear()
            servoMovement.stopFlag(ser)
            break

        if start == len(message):
            start = 0
            stop = 16
            start2 = 0
            stop2 = 0        
            
        elif stop == len(message):
            if start - stop == 15:
                start2 = 0
                stop2 = 1
            else:
                start += 1

                stop2 += 1
        else:
            start += 1
            stop += 1
        print start, stop, start2, stop2
        dispMessage = message[start:stop] + message[start2:stop2]
        lcd.message(dispMessage)
        print dispMessage
        time.sleep(0.3)
        if lcd.is_pressed(LCD.SELECT):
            lcd.clear()
            servoMovement.stopFlag(ser)
            break
        lcd.clear()
        time.sleep(0.1)

