import cv2
import numpy as np
def updateValue(x):
    pass
def findShape(video_device, shapes):
    Ret, frame = video_device.read()

    cv2.imshow("Main", frame)
    cv2.imshow("Canny", frame)

    """                               
    cv2.drawContours(a, shapes["beeSM"], -1, (0, 255, 0), 2)
    cv2.drawContours(b, shapes["hexagonSM"], -1, (0, 255, 0), 2)
    cv2.drawContours(c, shapes["moundSM"], -1, (0, 255, 0), 2)
    cv2.drawContours(d, shapes["squareSM"], -1, (0, 255, 0), 2)
    cv2.drawContours(e, shapes["teeSM"], -1, (0, 255, 0), 2)
    cv2.drawContours(f, shapes["triSM"], -1, (0, 255, 0), 2)
    cv2.drawContours(g, shapes["uSM"], -1, (0, 255, 0), 2)
    cv2.imshow("beeSM", a)
    cv2.imshow("hexagonSM", b)
    cv2.imshow("moundSM", c)
    cv2.imshow("squareSM", d)
    cv2.imshow("teeSM", e)
    cv2.imshow("triSM", f)
    cv2.imshow("uSM", g)
"""
    nr = 1
    cv2.createTrackbar("nr shapes", 'Main', nr, 10, updateValue)
    while True:
        nr = cv2.getTrackbarPos("nr shapes", "Main")
        if nr==0:
            nr+=1
        Ret, image = video_device.read()
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        canny = cv2.dilate(cv2.Canny(gray, 50, 200),None, iterations=1)
        cnts = np.array(cv2.findContours(canny.copy(), cv2.RETR_EXTERNAL,
                                cv2.CHAIN_APPROX_SIMPLE)[0])
        
        #if len(cnts) == 0:
        #    continue
        sortedContours = sorted(cnts, key=cv2.contourArea, reverse=True)
        result = dict()
        if len(sortedContours) != 0:
            sortedContours = sortedContours[0:nr]
        for contour in sortedContours:
            M = cv2.moments(contour)
            try:
                cX = int((M["m10"] / M["m00"]))
                cY = int((M["m01"] / M["m00"]))
            except:
                cX = 0
                cY = 0

            for key, value in shapes.iteritems():
                shape = cv2.matchShapes(contour, value[0], cv2.cv.CV_CONTOURS_MATCH_I1, 0)
                result[shape] = key

            #contour = contour.astype("float")
            #contour = contour.astype("int")
            cv2.circle(image, (cX, cY), 5, thickness=2, color=(255, 255, 0))
            mostSimilar = min(result.keys())
            name = result[mostSimilar]
            if mostSimilar > 2:
                #print min(result.keys())
                name = "not recognized"
            #print name, mostSimilar
            cv2.putText(image, name, (cX, cY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 255), 2)


        cv2.imshow("Main", image)
        cv2.imshow("Canny", canny)

        
        if (cv2.waitKey(10) & 0xFF == ord('q')):
            return name
        

    
