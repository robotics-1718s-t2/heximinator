import matchwithshapes, cv2, serial, tasks, servoMovement, time
import numpy as np

try:
    try:
        ser = serial.Serial(
            port='/dev/ttyAMA0',
            baudrate=115200,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            timeout=1)
    except:
        serial.Serial('/dev/ttyAMA0', 115200).close()
        ser = serial.Serial(
            port='/dev/ttyAMA0',
            baudrate=115200,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            timeout=1)
    video_device = cv2.VideoCapture(0)

    a=cv2.imread("circle.PNG")
    b=cv2.imread("letterE.PNG")
    c=cv2.imread("letterT.PNG")
    d=cv2.imread("numberONE.PNG")
    e=cv2.imread("square.PNG")
    f=cv2.imread("star.PNG")
    g=cv2.imread("triangle.PNG")

    shapes = {"circle" : cv2.Canny(cv2.cvtColor(a.copy(), cv2.COLOR_BGR2GRAY), 50, 200),
        "letterE" : cv2.Canny(cv2.cvtColor(b.copy(), cv2.COLOR_BGR2GRAY), 50, 200),
        "letterT" : cv2.Canny(cv2.cvtColor(c.copy(), cv2.COLOR_BGR2GRAY), 50, 200),
        "numberONE" : cv2.Canny(cv2.cvtColor(d.copy(), cv2.COLOR_BGR2GRAY), 50, 200),
        "square" : cv2.Canny(cv2.cvtColor(e.copy(), cv2.COLOR_BGR2GRAY), 50, 200),
        "star" : cv2.Canny(cv2.cvtColor(f.copy(), cv2.COLOR_BGR2GRAY), 50, 200),
        "triangle" : cv2.Canny(cv2.cvtColor(g.copy(), cv2.COLOR_BGR2GRAY), 50, 200)}

    for key, value in shapes.iteritems():
            shapes[key] = np.array(cv2.findContours(value, cv2.RETR_EXTERNAL,
                                 cv2.CHAIN_APPROX_SIMPLE)[0])


    servoMovement.idleStanding(ser)
    time.sleep(2)
    while True:
        shape = matchwithshapes.findShape(video_device, shapes)
        print shape
        tasks.chooseTask(ser, shape)


except KeyboardInterrupt:
    video_device.release()
    cv2.destroyAllWindows()
    servoMovement.letLoose(ser)
    ser.close()
