# Heximinator
![Alt text](https://upload.wikimedia.org/wikipedia/en/thumb/b/b9/Terminator-2-judgement-day.jpg/250px-Terminator-2-judgement-day.jpg)

[Proof of Concept video](https://owncloud.ut.ee/owncloud/index.php/s/0YZHL5ofASi6nNa)

# Team members:
+ Kert M�nnik
+ Robert Allik

# Overview:
+ Image trained circus robot using hexapod
+ Will perform various tasks after recognising a shape on a paper
+ Camera will probably be connected to the Raspberry Pi
+ We will try to use the Lynxmotion SSC-32 Servo Controller

# Tasks:
+ Walk in a fancy way
+ Wave flag using servo motor
+ Scroll song lyrics over LCD
+ Do LCD backlight disco
+ Rise one leg on one side
+ Rise one leg on both sides
+ Wave the flag and show text on the LCD

# Schedule:
| Deadline        | Topic           | Done  |
| ------------- |:-------------:| -----:|
| 12.04      | Project plan | YES |
| 19.04      | Assemble the robot | YES |
| 26.04      |   Image detection 50%    |   YES |
| 03.05 | Proof of concept + Image detection 100%?   |    YES |
| 10.05 | Walking + leg risings  |    YES |
| 18.05 | Project poster + Flag waving   |    YES |
| 21.05 | Project presentations with posters + LCD   |    YES |


# Component list:
| Item           | How many we need/have  |
|:-------------:| :-----:|
| Hexapod | Need 1, Have 1 |
| Camera | Need 1, Have 1 |
| Pi2 + servo controller |   Need 1+1, Have 1+1 |
| Battery |    Need 1, Have 1 |
| Flag servo |    Need 1, Have 1 |
| Pi power supply |    Need 1, Have 1 |
| LCD |    Need 1, Have 1 |
| Cables |    Need some, Have some |
| WiFi Dongle |    Need 1, Have 1 |

# Challenges and solutions:
+ Power. We have 3 power hungry components: Pi, Servos, and servo controller. All of them need 3 separate power supplies, because controller needs 9V and you can not power servos and Pi together, because power bank will be drained in few minutes, because servos take too much current. We have solved the problem: powering controller from a 9V battery, Pi from power bank, and servos from 11.4V battery with 6V level-shifter LM2596. We also needed to solder some wires to attach battery and level-shifter to the servos.
http://www.ti.com/lit/ds/symlink/lm2596.pdf
+ LCD mounting to the robot. We needed to mount the display on top of the robot, so we built a new platform for it. Does not look so good but it works :)
+ Assembly. Because the body of the robot is made of metal, we needed to add isolation tape on the bottom of the Pi and LCD display to prevent short-circuiting them. Also, adding things to the robot was achieved by using cable ties and bolts and spacers. Next we will add flag servo to the robot. Will be placed on top of the robot.
+ Walking. At first, had trouble connecting Pi and servo controller, but controller's datasheet helped out to connect the RX and TX wires. Now it is working in unidirectional TTL Serial mode and we can send commands to move servos. Next week we will try to get multiple legs moving at once. We can achieve that by controlling multiple legs at once thanks to grouping which can be done by sending a command to controller.
http://www.lynxmotion.com/images/html/build136.htm#comform
+ Future assembly. Will connect camera and flag servo to the Heximinator. Will be placed on top of the robot.
+ Poster. Have several ideas for poster and we will have a template.
+ Everything working together. We will see how everything works together, but there should not be any big setbacks. The main flow has to be perfectly written.